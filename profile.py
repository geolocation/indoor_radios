#!/usr/bin/python

"""
Two SDR nodes running srsLTE software with SDR hardware. You may request
that it runs over the air, or within the controlled (PhantomNet) environment.

Specifically srsGUI and srsLTE code is pre-installed:

https://github.com/srsLTE/srsGUI

https://github.com/srsLTE/srsLTE

Instructions:

**To run the EPC**

Open a terminal on the epc-enb node in your experiment. (Go to the "List View"
in your experiment. If you have ssh keys and an ssh client working in your
setup you should be able to click on the black "ssh -p ..." command to get a
terminal. If ssh is not working in your setup, you can open a browser shell
by clicking on the Actions icon corresponding to the node and selecting Shell
from the dropdown menu.)

Start up the EPC:

    sudo srsepc
    
**To run the eNodeB**

Open another terminal on the epc-enb node in your experiment.

Start up the eNodeB:

    sudo srsenb

**To run the UE**

Open a terminal on the ue node in your experiment.

Start up the UE:

    sudo srsue

**Verify functionality**

Open another terminal on the ue node in your experiment.

Verify that the virtual network interface tun_srsue" has been created:

    ifconfig tun_srsue

Run ping to the SGi IP address via your RF link:
    
    ping 172.16.0.1

Killing/restarting the UE process will result in connectivity being interrupted/restored.

If you are using an ssh client with X11 set up, you can run the UE with the GUI
enabled to see a real time view of the signals received by the UE:

    sudo srsue --gui.enable 1


"""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
import geni.rspec.igext as ig
#import geni.rspec.emulab.spectrum as spectrum



b210_node_disk_image = \
        "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"

setup_command = "/local/repository/startup.sh"



def b210_nuc_pair(idx, b210_node, installs):
    node_name = b210_node.node_name.format(idx=idx)
    b210_nuc_pair_node = request.RawPC(b210_node.node_name.format(
        idx=idx))
    b210_nuc_pair_node.component_id = b210_node.component_id
    b210_nuc_pair_node.Desire( "rf-radiated", 1 ) 

    b210_nuc_pair_node.disk_image = b210_node_disk_image

    service_command = " ".join([setup_command] + installs)
    b210_nuc_pair_node.addService(
        rspec.Execute(shell="bash", command=service_command))


nuc13 = {"component_id": "nuc13","node_name": "b210-node-0"}
nuc14 = {"component_id": "nuc14","node_name": "b210-node-1"}

portal.context.defineStructParameter("b210_nodes", "Add Node", [nuc13, nuc14],
                                     multiValue=True,
                                     itemDefaultValue=
                                     {"component_id": "nucX",
                                      "node_name": "b210-node-{idx}"},
                                     min=0, max=None,
                                     members=[
                                         portal.Parameter(
                                             "component_id",
                                             "nuc{idx}",
                                             portal.ParameterType.STRING, ""),
                                         portal.Parameter(
                                             "node_name",
                                             "Node name will be formatted "
                                             "with idx",
                                             portal.ParameterType.STRING,
                                             "")
                                     ],
                                    )




portal.context.defineParameter("install_srslte",
                               "Should srsLTE Radio be installed?",
                               portal.ParameterType.BOOLEAN, True)
portal.context.defineParameter("install_gnuradio",
                               "Should GNU Radio (where uhd_fft, uhd_siggen, "
                               "etc) come from be installed?",
                               portal.ParameterType.BOOLEAN, True)
portal.context.defineParameter("install_gnuradio_companion",
                               "Should GNU Radio Companion be installed?",
                               portal.ParameterType.BOOLEAN, True)



params = portal.context.bindParameters()

request = portal.context.makeRequestRSpec()

installs = []



if params.install_srslte:
    installs.append("srslte")

if params.install_gnuradio:
    installs.append("gnuradio")

if params.install_gnuradio_companion:
    installs.append("gnuradio-companion")

for i, b210_node in enumerate(params.b210_nodes):
    b210_nuc_pair(i, b210_node, installs)


portal.context.printRequestRSpec()
